# Alpine is lightweight minimal image that run python 
FROM python:3.7-alpine
MAINTAINER BelialDaniel

ENV PYTHONUNBUFFERED 1

RUN mkdir /opt/app
WORKDIR /opt/app

# from main folder app to dokker container
COPY ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt

COPY ./app .

# This is used for security purposes
#  if this is not used, the app runs in with the root account
RUN adduser -D user
USER user