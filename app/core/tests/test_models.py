from django.test import TestCase
from django.contrib.auth import get_user_model

from core.models import Tag


class ModelsTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            email='test@python.com',
            password='1234567890'
        )

    def test_create_user_with_email_successful(self):
        """
            Test creates a new user successful
        """
        email = 'belialdaniel@python.com'
        password = '1234'

        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )

        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        email = "belialdaniel@PYTHON.com"

        user = get_user_model().objects.create_user(
            email=email, 
            password='1234'
        )

        self.assertEqual(user.email, email.lower())
    
    def test_new_user_ivalid_email(self):
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, '1234')

    def test_create_new_superuser(self):
        user = get_user_model().objects.create_superuser(
            email='belialdaniel@python.com', 
            password='1234'
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
    
    def test_tag_str(self):
        """Test the tag string representation"""
        tag = Tag.objects.create(
            user=self.user,
            name='Vegan'
        )

        self.assertEqual(str(tag), tag.name)

    def test_ingredient_str(self):
        """Test the ingredient string representation"""
        ingredient = models.Ingredient.objects.create(
            user=sample_user(),
            name='Cucumber'
        )
        self.assertEqual(str(ingredient), ingredient.name)