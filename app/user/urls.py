from django.urls import path 
from .views import CreateUserView, CreateTokenView, ManageUserView

# This help us to use reverse to get the path url
app_name = 'user'

urlpatterns = [                             # name is used to helps reverse to create the url
    path('create/', CreateUserView.as_view(), name='create'),
     path('token/', CreateTokenView.as_view(), name='token'),
     path('me/', ManageUserView.as_view(), name='me'),
]