from django.contrib.auth import get_user_model, authenticate
from rest_framework import serializers

# The ModelSerializer class is the same as a regular Serializer class, except that:
#  * It will automatically generate a set of fields for you, based on the model.
#  * It will automatically generate validators for the serializer, such as unique_together validators.
#  * It includes simple default implementations of .create() and .update().

class UserSerializer(serializers.ModelSerializer):
    """Serializer for the users object"""

    class Meta:
        # get_user_model() return the class
        model = get_user_model()
        fields = ('email', 'password', 'name')
        # This extra_kwargs helps us to make constrains to speficied field
        extra_kwargs = {'password': {'write_only': True, 'min_length': 5}}
        # fields = '__all__' includes all fields
        # exclude = ('password', ) exclude fields in the serializer
        # Since version 3.3.0, it is mandatory to provide one of the attributes fields or exclude.

    def create(self, validated_data):
        """Create a new user with encrypted password and return it"""
        return get_user_model().objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        """Update a user, setting the password correctly and return it"""
        password = validated_data.pop('password', None)
        user = super().update(instance, validated_data)
    
        if password:
            user.set_password(password)
            user.save()
    
        return user

class AuthTokenSerializer(serializers.Serializer):
    # These fields are displayed in the endpoint api
    email = serializers.CharField(label="Username")
    password = serializers.CharField(
        label="Password",
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        username = attrs.get('email')
        password = attrs.get('password')

        if username and password:
            user = authenticate(
                request=self.context.get('request'),
                username=username, 
                password=password
            )

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                msg = 'Unable to log in with provided credentials.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Must include "username" and "password".'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs # we most return the attrs when all is success