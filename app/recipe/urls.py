from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import TagViewSet, IngredientViewSet


router = DefaultRouter()
router.register('tags', TagViewSet)
router.register('ingredients', IngredientViewSet)
# this give us an url like : /recipe/tags/1

app_name = 'recipe'

urlpatterns = [
    path('', include(router.urls)),
]